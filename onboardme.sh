#!/bin/sh

while true; do
  read -p "Install Node 8? " yn
    case $yn in
      [Yy]* ) npm install -g n; sudo n 8; break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no";;
    esac
done

while true; do
  read -p "Install AWS command-line tools? " yn
    case $yn in
      [Yy]* ) brew install python3; brew postinstall python3; pip3 install awscli --upgrade --user; break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no";;
    esac
done

current_location=$PWD
while true; do
  read -p "Clone Token of Trust repositories? " yn
    case $yn in
      [Yy]* ) cd ${current_location%/team}; 
              git clone https://squirrelsbo@bitbucket.org/squirrelsbo/team.git; 
              git clone https://squirrelsbo@bitbucket.org/squirrelsbo/process-image.git; 
              break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no";;
    esac
done

while true; do
  read -p "Add Token of Trust profile (used for helpful scripts)? " yn
    case $yn in
      [Yy]* ) echo "export TOT_HOME=${current_location%/team}\nsource $current_location/.profile" >> ~/.profile; break;;
      [Nn]* ) break;;
      * ) echo "Please answer yes or no";;
    esac
done

